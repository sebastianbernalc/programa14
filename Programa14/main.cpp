#include <iostream>

using namespace std;

int main()
{
    int Matriz[5][5]={{1,2,3,4,5},{6,7,8,9,10},{11,12,13,14,15},{16,17,18,19,20},{21,22,23,24,25}};
    cout<<"** ORIGINAL ***\n";
    for (int i=0;i<5;i++){      //Imprime la matriz original posicion por posicion
        for (int c=0;c<5;c++){
            cout<<Matriz[i][c]<<"  ";
        }cout<<endl;
    }

    cout<<endl;
    cout<<"*** 90 GRADOS ***\n";  //Para invertir la matriz o girarla en 90 grados bastan con juego de posiciones en la matriz con filas y columnas pasando las filas a columnas y viceversa con sus valores del utimo al final en el caso de cambio de columna a fila
    for(int i=0;i<5;i++){
        for (int c=4;c>=0;c--){

            cout<<Matriz[c][i]<<"  ";
        }cout<<endl;
    }
    cout<<endl;
    cout<<"*** 180 GRADOS ***\n";  //Realmente en esta parte tambien hacemos los mismos pasos del anterior pero en este caso la matriz no esta modificada en 90 grados asi que tambien jugamos con las posiciones para cuadrar los valores en sus respectivas posiciones
    for(int i=4;i>=0;i--){      //EN el cual tomamos desde la ultima fila y la inevertimos sus valores de atras hacia adelante y colocarlas en la parte superior
        for(int c=4;c>=0;c--){
            cout<<Matriz[i][c]<<"  ";
        }cout<<endl;
    }
    cout<<endl;
    cout<<"*** 270 GRADOS ***\n";       //Y para este tomamos desde la ultima columna y la ponesmo de primera fila con sus valores ordenados
    for(int i=4;i>=0;i--){
        for(int c=0;c<5;c++){
            cout<<Matriz[c][i]<<"  ";
        }cout<<endl;
    }
    return 0;
}

